<?php
namespace Core;
use PDO;
use PDOException;
class DataBase
{
    public function getDataBase()
    {
        $conf = include_once __DIR__."/../app/database.php";
        
        if($conf['dirver'] == 'sqlite'){
            $sqlite = __DIR__."/../storage/database/".$conf['sqlite']['host'];
            $sqlite = "sqlite:".$sqlite;
            

            try{
                $pdo = new PDO($sqlite);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                return $pdo;
            }catch(PDOException $e){
                echo $e->getMessage();
            }
        }elseif($conf['driver'] == 'mysql'){
            $host = $confi['mysql']['host'];
            $db = $confi['mysql']['database'];
            $user = $confi['mysql']['user'];
            $pass = $confi['mysql']['pass'];
            $charset = $confi['mysql']['charset'];
            $collation = $confi['mysql']['collation'];

            try{
                $pdo = new PDO("mysql:host=$host; dbname=$db; chatset=$charset",$user,$pass);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND,"SET NAMES '$charset' COLLATE '$collation'");
                $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                return $pdo;
            }catch(PDOException $e){
                echo $e->getMessage();
            }
        }
    }
}