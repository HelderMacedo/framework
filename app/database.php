<?php

return [
    /**
     * Options (mysql, sqlite)
     */
    'driver' => 'mysql',
    'sqlite' => [
        'host' => 'database.db',
        
    ],

    'mysql' =>[
        'host' => 'localhost',
        'database' => 'curso_microframework',
        'user' => 'root',
        'pass' => '32051217',
        'charset' => 'utf8',
        'collation' => 'ut8_unicode_ci'
    ]
];